#include <iostream>
#include "screen.h"
namespace Endless
{
	namespace screen
	{
		Textures textures;
		STATES gameState;
		void init() 
		{
			gameState = Menu;
			textures.scrolling1 -= 0.0f;
			textures.scrolling2 -= 0.0f;
			textures.scrolling3 -= 0.0f;
			textures.scrolling4 -= 0.0f;
			textures.scrolling5 -= 0.0f;
			textures.scrolling6 -= 0.0f;
			loadTexture();
		}
		
		void update()
		{
			switch (gameState)
			{
			case Menu: 
				if (IsKeyPressed(KEY_ENTER))
				{
					gameState = Playing;

				}
				else if (IsKeyPressed(KEY_C))
				{
					gameState = Config;
				}
				else if (IsKeyPressed(KEY_F))
				{
					gameState = Credits;
				}
				break;
			case Config:
				if (IsKeyPressed(KEY_M))
				{
					gameState = Menu;
				}
				break;
			case Credits:
				if (IsKeyPressed(KEY_M))
				{
					gameState = Menu;
				}
				break;
			case Playing:
				textures.scrolling1 -= 0.1f;
				textures.scrolling2 -= 0.1f;
				textures.scrolling3 -= 0.5f;
				textures.scrolling4 -= 0.5f;
				textures.scrolling5 -= 1.0f;
				textures.scrolling6 -= 1.0f;
				if (textures.scrolling1 <= -textures.background1.width && textures.scrolling2 <= -textures.background2.width)
				{
					textures.scrolling1 = 0.0f;
					textures.scrolling2 = 0.0f;
				}
				if (textures.scrolling3 <= -textures.background3.width && textures.scrolling4 <= -textures.background4.width)
				{
					textures.scrolling3 = 0.0f;
					textures.scrolling4 = 0.0f;
				}
				if (textures.scrolling5 <= -textures.background5.width && textures.scrolling6 <= -textures.background6.width)
				{
					textures.scrolling5 = 0.0f;
					textures.scrolling6 = 0.0f;
				}

				if (IsKeyPressed(KEY_P))
				{
					gameState = Pause;
				}
				break;
			case Pause:
				if (IsKeyPressed(KEY_ENTER))
				{
					gameState = Playing;
				}
				else if (IsKeyPressed(KEY_M))
				{
					gameState = Menu;
					reInit();
				}
				break;
			case Victory:
				break;
			case Defeat:
				break;
			default:
				break;
			}
			
		}
		void draw() 
		{
			if (gameState == Playing || gameState == Pause)
			{
				//1 & 2 twice
				DrawTextureEx(textures.background1, Vector2{ textures.scrolling1, 20 }, 0.0f, 1.0f, WHITE);
				DrawTextureEx(textures.background1, Vector2{ textures.background1.width + textures.scrolling1, 20 }, 0.0f, 1.0f, WHITE);

				DrawTextureEx(textures.background2, Vector2{ textures.scrolling2, 20 }, 0.0f, 1.0f, WHITE);
				DrawTextureEx(textures.background2, Vector2{ textures.background2.width + textures.scrolling2, 20 }, 0.0f, 1.0f, WHITE);
				//3 & 4 twice
				DrawTextureEx(textures.background3, Vector2{ textures.scrolling3, 20 }, 0.0f, 1.0f, WHITE);
				DrawTextureEx(textures.background3, Vector2{ textures.background3.width + textures.scrolling3, 20 }, 0.0f, 1.0f, WHITE);

				DrawTextureEx(textures.background4, Vector2{ textures.scrolling4, 20 }, 0.0f, 1.0f, WHITE);
				DrawTextureEx(textures.background4, Vector2{ textures.background4.width + textures.scrolling4, 20 }, 0.0f, 1.0f, WHITE);
				//5 & 6 twice
				DrawTextureEx(textures.background5, Vector2{ textures.scrolling5, 70 }, 0.0f, 1.0f, WHITE);
				DrawTextureEx(textures.background5, Vector2{ textures.background5.width + textures.scrolling5, 70 }, 0.0f, 1.0f, WHITE);

				DrawTextureEx(textures.background6, Vector2{ textures.scrolling6, 70 }, 0.0f, 1.0f, WHITE);
				DrawTextureEx(textures.background6, Vector2{ textures.background6.width + textures.scrolling6, 70 }, 0.0f, 1.0f, WHITE);
				if (gameState == Pause) 
				{
					DrawText("Play[Press Enter]", GetScreenWidth() / 2, GetScreenHeight() * 4 / 8, GetScreenWidth() / 40, GREEN);
					DrawText("Menu[Press M]", GetScreenWidth() / 2, GetScreenHeight() * 5 / 8, GetScreenWidth() / 40, GREEN);
				}
				
			}
			else if (gameState == Menu)
			{
				DrawText("ENDLESS", GetScreenWidth() / 2, GetScreenHeight() / 4, GetScreenWidth() / 30, RED);
				DrawText("Play[Press Enter]", GetScreenWidth() / 2, GetScreenHeight() * 4 / 8, GetScreenWidth() / 40, GREEN);
				DrawText("Config[Press C]", GetScreenWidth() / 2, GetScreenHeight() * 5 / 8, GetScreenWidth() / 40, GREEN);
				DrawText("Credits[Press F]", GetScreenWidth() / 2, GetScreenHeight() * 6 / 8, GetScreenWidth() / 40, GREEN);
				DrawText("Exit[Press ESC]", GetScreenWidth() / 2, GetScreenHeight() * 7 / 8, GetScreenWidth() / 40, GREEN);
			}
			else if (gameState == Credits)
			{
				DrawText("Menu[Press M]", GetScreenWidth() * 5 / 8, GetScreenHeight() * 7 / 8, GetScreenWidth() / 40, GREEN);
			}
			else if (gameState == Config)
			{
				DrawText("Menu[Press M]", GetScreenWidth() * 5 / 8, GetScreenHeight() * 7 / 8, GetScreenWidth() / 40, GREEN);
			}
			
		}
		void deInit() 
		{
			unloadTexture();
		}

		void loadTexture()
		{
			textures.background1 = LoadTexture("res/BackGround/backGround_1.png");
			textures.background2 = LoadTexture("res/BackGround/backGround_2.png");
			textures.background3 = LoadTexture("res/BackGround/backGround_3.png");
			textures.background4 = LoadTexture("res/BackGround/backGround_4.png");
			textures.background5 = LoadTexture("res/BackGround/backGround_5.png");
			textures.background6 = LoadTexture("res/BackGround/backGround_6.png");
		}

		void unloadTexture()
		{
			UnloadTexture(textures.background1);
			UnloadTexture(textures.background2);
			UnloadTexture(textures.background3);
			UnloadTexture(textures.background4);
			UnloadTexture(textures.background5);
			UnloadTexture(textures.background6);
		}
		void reInit() 
		{

		}
	}
}