#include "gameplay.h"
namespace Endless 
{
	namespace gameplay 
	{
		void init()
		{
			SetTargetFPS(60);
			screen::init();
			player::init();
			obstacle::init();
		}
		void update()
		{
			screen::update();
			player::update();
			obstacle::update();
		}
		void draw()
		{
			BeginDrawing();
			ClearBackground(GetColor(0x052c46ff));
			screen::draw();
			player::draw();
			obstacle::draw();
			EndDrawing();
		}
		void deInit()
		{
			screen::deInit();
		}
		void game() 
		{
			update();
			draw();
		}

	}
}