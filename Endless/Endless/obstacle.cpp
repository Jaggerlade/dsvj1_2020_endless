#include "obstacle.h"
namespace Endless 
{
	namespace obstacle 
	{
		void randomPos(float randomPosX);
		Obstacle obstacleArray[MAXOBSTACLES];
		void init()
		{
			int randomPath;
			int randomType;
			float randomPosX = 0;
		
			for (int i = 0; i < MAXOBSTACLES; i++)
			{				
				
				randomPath = GetRandomValue(1,3);
				randomType = GetRandomValue(1, 3);
				switch (randomType)
				{
				case 1:
					obstacleArray[i].obstacleType = Destructible;
					break;
				case 2:
					obstacleArray[i].obstacleType = Dodgable;
					break;
				case 3:
					obstacleArray[i].obstacleType = Undodgable;
					break;
				default:
					break;
				}
				
				switch (randomPath)
				{
				case 1:
					obstacleArray[i].objectsRec = { static_cast<float>(GetScreenWidth()),player::player.top,
										static_cast<float> (GetScreenWidth() / 32),static_cast<float> (GetScreenHeight() / 18) };
					
					break;
				case 2:
					obstacleArray[i].objectsRec = { static_cast<float>(GetScreenWidth()),player::player.mid,
										static_cast<float> (GetScreenWidth() / 32),static_cast<float> (GetScreenHeight() / 18) };
					break;
				case 3:
					obstacleArray[i].objectsRec = { static_cast<float>(GetScreenWidth()),player::player.bot,
										static_cast<float> (GetScreenWidth() / 32),static_cast<float> (GetScreenHeight() / 18) };
					break;
				default:
					break;
				}
				randomPos(randomPosX); 
			
				
			}	
		}

		void update()
		{
			if (screen::gameState == screen::Playing)
			{
				for (int i = 0; i < MAXOBSTACLES; i++)
				{
					obstacleArray[i].objectsRec.x -= 5;
				}
			}
			

		}

		void draw()
		{
			/*DrawRectangleRec(obstacleDodgable.objectsRec,BLUE);
			DrawRectangleRec(obstacleUndestructible.objectsRec, GREEN);
			DrawRectangleRec(obstacleDestructible.objectsRec, RED);*/
			if (screen::gameState == screen::Playing || screen::gameState == screen::Pause)
			{
				for (int i = 0; i < MAXOBSTACLES; i++)
				{
					switch (obstacleArray[i].obstacleType)
					{
					case Destructible:
						DrawRectangleRec(obstacleArray[i].objectsRec, BLUE);
						break;
					case Dodgable:
						DrawRectangleRec(obstacleArray[i].objectsRec, RED);
						break;
					case Undodgable:
						DrawRectangleRec(obstacleArray[i].objectsRec, GREEN);
						break;
					default:
						break;
					}

				}
			}
			
		}

		void deInit()
		{

		}
		void loadTexture()
		{
			
		}
		void UnloadTexture()
		{
		}
		void randomPos(float randomPosX) 
		{
			for (int j = 0; j < MAXOBSTACLES; j++)
			{
				do
				{
					randomPosX = static_cast<float>(GetRandomValue(GetScreenWidth(), GetScreenWidth() + GetScreenWidth() * 3));
					obstacleArray[j].objectsRec.x = randomPosX;
				} while (CheckCollisionRecs(obstacleArray[j].objectsRec, obstacleArray[j + 1].objectsRec));
				

				/*bool collision = false;
				while (collision == false)
				{
					for (int k = 0; k < MAXOBSTACLES; k++)
					{
						if (CheckCollisionRecs(obstacleArray[j].objectsRec, obstacleArray[k].objectsRec))
						{
							collision = true;
						}
					}
				}*/
			}
		}

	}
}
