#ifndef GAMEPLAY_H
#define GAMEPLAY_H
#include <iostream>
#include "screen.h"
#include "player.h"
#include "obstacle.h"
namespace Endless
{
	namespace gameplay	
	{
		void init();
		void update();
		void draw();
		void deInit();
		void game();
	}
}
#endif // !GAMEPLAY_H
