#ifndef SCREEN_H
#define SCREEN_H
#include <iostream>
#include "raylib.h"
namespace Endless 
{
	namespace screen 
	{
		enum STATES
		{
			Menu,
			Config,
			Credits,
			Playing,
			Pause,
			Victory,
			Defeat
		};
		struct Textures
		{
			Texture2D background1;
			Texture2D background2;
			Texture2D background3;
			Texture2D background4;
			Texture2D background5;
			Texture2D background6;
			float scrolling1;
			float scrolling2;
			float scrolling3;
			float scrolling4;
			float scrolling5;
			float scrolling6;
		};
		extern Textures textures;
		extern STATES gameState;
		void init();
		void update();
		void draw();
		void deInit();
		void loadTexture();
		void unloadTexture();
		void reInit();
	}
}
#endif // !SCREEN_H
