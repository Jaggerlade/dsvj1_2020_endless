#ifndef OBSTACLE_H
#define OBSTACLE_H
#define MAXOBSTACLES 20
#include "raylib.h"
#include "player.h"
#include "screen.h"
namespace Endless 
{
	namespace obstacle 
	{
		enum OBSTACLETYPE
		{
			Destructible,
			Dodgable,
			Undodgable
		};
		struct Obstacle
		{
			Rectangle objectsRec;
			OBSTACLETYPE obstacleType;
			int speed;

		};
		
		extern Obstacle obstacleDodgable;
		extern Obstacle obstacleUndestructible;
		extern Obstacle obstacleDestructible;
		extern Obstacle obstacleArray[MAXOBSTACLES];
		void init();
		void update();
		void draw();
		void deInit();
		void loadTexture();
		void UnloadTexture();
	}
}
#endif // !OBSTACLE_H

