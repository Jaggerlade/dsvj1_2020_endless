#include "player.h"
#include <iostream>
using namespace std;
namespace Endless 
{
	
	namespace player 
	{
		Player player;
		void init()
		{
			player.rec = { static_cast<float>(GetScreenWidth() / 9),static_cast<float>(GetScreenHeight() / 2), 
						   static_cast<float> (GetScreenWidth() / 32),static_cast<float> (GetScreenHeight() / 18)};
			player.bot = static_cast<float>(GetScreenHeight() / 1.4f);
			player.mid = static_cast<float>(GetScreenHeight() /2.0f);
			player.top = static_cast<float>(GetScreenHeight() / 3.1f);

			player.playerScore = 0;

			loadTexture();
			
			
		}

		void update()
		{
			{
			if (screen::gameState == screen::Playing) 
				inputMovimiento();
			}
			
		}

		void draw()
		{
			if (screen::gameState == screen::Playing || screen::gameState == screen::Pause)
			{
				DrawTexture(player.playerTexture, static_cast<int>(player.rec.x),static_cast<int>(player.rec.y), WHITE);
				DrawText(TextFormat("SCORE: %i", player::player.playerScore), GetScreenWidth() * 6 / 8, GetScreenHeight() * 1 / 8, GetScreenWidth() / 40, GREEN);
			}
		}

		void deInit()
		{

		}

		void loadTexture()
		{
			Image reSize;
			reSize = LoadImage("res/Character/Player.png");
			ImageResize(&reSize,static_cast<int> (player.rec.width), static_cast<int>(player.rec.height));
			player.playerTexture = LoadTextureFromImage(reSize);
			UnloadImage(reSize);
		}

		void unloadTexture()
		{
			UnloadTexture(player.playerTexture);
		}

		void inputMovimiento() 
		{
			if (screen::gameState == screen::Playing)
			{
				if (player.rec.y == player.top && IsKeyPressed(KEY_S))
				{
					player.rec.y = player.mid;
				}
				else if (player.rec.y == player.mid && IsKeyPressed(KEY_S))
				{
					player.rec.y = player.bot;
				}
				else if (player.rec.y == player.mid && IsKeyPressed(KEY_W))
				{
					player.rec.y = player.top;
				}
				else if (player.rec.y == player.bot && IsKeyPressed(KEY_W))
				{
					player.rec.y = player.mid;
				}
			}
		}
	}
}

