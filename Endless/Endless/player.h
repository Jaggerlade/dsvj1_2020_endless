#pragma once
#ifndef PLAYER_H
#define PLAYER_H
#include "raylib.h"
#include "screen.h"
namespace Endless 
{
	namespace player
	{
		struct Player
		{
			Texture2D playerTexture;
			Rectangle rec;
			float top;
			float mid;
			float bot;
			int playerScore;
		};
		extern Player player;
		void init();
		void update();
		void draw();
		void deInit();
		void loadTexture();
		void unloadTexture();
		void inputMovimiento();
	}
}

#endif // !PLAYER_H
