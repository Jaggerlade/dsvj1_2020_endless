#include <iostream>
#include "gameplay.h"
using namespace std;
using namespace Endless;
int main() 
{
	InitWindow(1920, 1080, "raylib [textures] example - background scrolling");
	gameplay::init();
	while (!WindowShouldClose())
	{
		gameplay::game();
	}
	gameplay::deInit();
	CloseWindow();
	return 0;
}